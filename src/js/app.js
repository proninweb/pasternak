"use strict";

const jq = jQuery;

var Pasternak = (function() {
    var is_fix = false;

    const BREAKPOINTS = {
      mobile: 760,
      tablet: 991
    };

    var init = function() {
        _listeners();
    };

    var _onLoad = function() {
        _stickyHeader();

        if (jq('.js_mainslider').length) {
            jq('.js_mainslider').owlCarousel({
                loop: true,
                nav: true,
                dots: false,
                items: 1,
                autoplay: false,
                autoplayTimeout: 4000,
                autoplayHoverPause: true,
                navText: ['', ''],
                onInitialized: _mainslider,
                onChanged: _mainslider,
                thumbs: false
            });
        }

        if (jq('.js_reviews').length) {
            jq('.js_reviews').owlCarousel({
                loop: true,
                nav: true,
                dots: false,
                items: 1,
                autoplay: false,
                autoplayTimeout: 4000,
                autoplayHoverPause: true,
                navText: ['', ''],
                thumbs: true,
                thumbImage: false,
                thumbsPrerendered: true
            });
        }

        if (jq('.js_imgslider').length) {
            jq('.js_imgslider').each(function(i, item) {
                jq(item).owlCarousel({
                    loop: true,
                    nav: true,
                    dots: true,
                    items: 1,
                    navText: ['', ''],
                    thumbs: false
                });
            });
        }

        if (jq('.js_prodslider').length) {
            jq('.js_prodslider').each(function (i, item) {
                jq(item).owlCarousel({
                    loop: true,
                    nav: true,
                    dots: false,
                    thumbs: false,
                    margin: 30,
                    navText: ['', ''],
                    responsive: {
                        0: {
                            items: 1
                        },
                        480: {
                            items: 2
                        },
                        992: {
                            items: 3
                        }
                    }
                });
            });
        }

        if (jq('.js_timeslider').length) {
            jq('.js_timeslider').each(function (i, item) {
                jq(item).owlCarousel({
                    loop: false,
                    nav: false,
                    dots: false,
                    thumbs: false,
                    center:true,
                    margin: 30,
                    navText: ['', ''],
                    responsive: {
                        0: {
                            items: 1
                        },
                        480: {
                            items: 2
                        },
                        992: {
                            items: 4
                        }
                    }
                });

                if (jq(screen).width() > BREAKPOINTS.tablet) {
                    _timeslider(jq(item));
                }
            });
        }

        if (jq('.js_catpreview').length) {
            jq('.js_catpreview').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                fixedContentPos: true,
                fixedBgPos: true,
                removalDelay: 300,
                mainClass: 'mfp-no-margins mfp-with-zoom',
                image: {
                    verticalFit: true
                },
                zoom: {
                    enabled: true,
                    duration: 300
                }
            });
        }

        if (jq('.js_modal').length) {
            jq('.js_modal').magnificPopup({
                type: 'inline',

                fixedContentPos: false,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,

                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });
        }

        if (jq('.js_phone').length) {
            jq('.js_phone').mask('+7 (999) 999-99-99');
        }

        if (jq('.js_callform').length) {
            var login = jq('.js_callform');

            var call_validation = login.validate({
                rules: {
                    inp_name: {
                        required: true
                    },
                    inp_phone: {
                        required: true
                    }
                },
                messages: {
                    inp_name: '',
                    inp_phone: ''
                },
                submitHandler: function (form) {
                    // Тут отправляем данные на сервер
                }
            });
        }

        if (jq('.js_catalogform').length) {
            var catalog = jq('.js_catalogform');

            var catalog_validation = catalog.validate({
                rules: {
                    inp_name: {
                        required: true
                    },
                    inp_phone: {
                        required: true
                    },
                    inp_mail: {
                        required: true,
                        email: true
                    },
                    inp_check: {
                        required: true
                    }
                },
                messages: {
                    inp_name: '',
                    inp_phone: ''
                },
                submitHandler: function (form) {
                    // Тут отправляем данные на сервер
                }
            });
        }

        if (jq('.js_letterform').length) {
            var letter = jq('.js_letterform');

            var letter_validation = letter.validate({
                rules: {
                    letter_name: {
                        required: true
                    },
                    letter_text: {
                        required: true
                    },
                    letter_phone: {
                        required: true
                    },
                    letter_mail: {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    letter_name: '',
                    letter_text: '',
                    letter_phone: '',
                    letter_mail: ''
                },
                showErrors: function(errorMap, errorList) {
                    this.defaultShowErrors();
                    jq('label.error').remove();
                },
                submitHandler: function (form) {
                    // Тут отправляем данные на сервер
                }
            });
        }

        if (jq('.js_preview').length) {
            jq('.js_preview').get(0).click();
        }

        if (jq('.js_filter').length) {
            jq('.js_filter').get(0).click();
        }

        if (jq('.js_prodbox').length) {
            _stickyProduct();
        }

        _mobile();
    };

    var _scroller = function(e) {
        e.preventDefault();
        var scroll_el = jq(this).attr('href');
        if (jq(scroll_el).length) {
            jq('html, body').animate({ scrollTop: jq(scroll_el).offset().top - 100 }, 500);
        }
    };

    var _onScroll = function() {
        _stickyHeader();

        if (jq('.js_prodbox').length) {
            _stickyProduct();
        }
    };

    var _stickyHeader = function() {
        var top = jq(window).scrollTop(),
            header_top = jq('.js_headertop'),
            header_main = jq('.js_headermain'),
            header = jq('header');

        if (jq(window).width() > BREAKPOINTS.tablet) {

            if (top > header_top.height()) {
                if (!is_fix) {
                    header.addClass('fix');
                    header_top.css('padding-bottom', header_main.innerHeight());
                    is_fix = true;
                }
            } else {
                if (is_fix) {
                    header.removeClass('fix');
                    header_top.css('padding-bottom', 0);
                    is_fix = false;
                }
            }
        } else {
            header.removeClass('fix');
            header_top.css('padding-bottom', 0);
            is_fix = false;
        }
    };

    var _stickyProduct = function() {
      var obj = jq('.js_product'),
          box = jq('.js_prodbox'),
          wrap = jq('.ui__product_wrapper'),
          box_top = box.offset().top,
          top = jq(window).scrollTop(),
          tmp = 0;

      if (jq(window).width() > BREAKPOINTS.tablet) {
          if ((top > box_top - 40) && (top < wrap.offset().top + wrap.height() - obj.height() - 150)) {
              tmp = top - box_top + 55;
              obj.css('position', 'absolute');
              obj.css('top', tmp + 'px');
          } else if (top >= wrap.offset().top + wrap.height() - obj.height() - 150) {
            // nothing
          } else if (top <= box_top - 40) {
              obj.css('position', '');
              obj.css('top', '0');
              tmp = 0;
          }
      } else {
          obj.css({'top': '0', 'position': ''});
          tmp = 0;
      }
    };

    var _mainslider = function(e) {
        var pages = e.item.count,
            page = (e.type === 'initialized') ? 1 : (e.item.index + 1) - e.relatedTarget._clones.length / 2;

        if (page > pages || page === 0) {
            page = pages - (page % pages);
        }

        jq('.js_mainslider_cur').text(page);
        jq('.js_mainslider_all').text('/ ' + pages);
    };

    var _timeslider = function(obj) {
      var container = jq('.grid--container'),
          win_w = jq(window).width(),
          delta = Math.round((win_w - container.width()) / 2);

      setTimeout(function() {
          obj.find('.owl-stage').css('transform', 'translate3d(' + delta + 'px, 0px, 0px)');
      }, 150);
    };

    var _preview = function(e) {
      e. preventDefault();
      var obj = jq(this),
          target = obj.attr('href'),
          img = obj.data('img');

      jq('.js_preview').removeClass('active');
      obj.addClass('active');

      if (jq(target).length) {
          jq(target).attr('src', img);
      } else {
          console.error('Target not found!');
      }
    };

    var _filter = function(e) {
      e.preventDefault();

      var obj = jq(this),
          target = obj.attr('href');

      jq('.js_filter').removeClass('active');
      obj.addClass('active');

      jq('.js_filtertarget').removeClass('active');
      jq('.js_filtertarget[data-filter="' + target.slice(1) + '"]').addClass('active');
    };

    var _mtoggle = function(e) {
        //e.preventDefault();
        var obj = jq(this);

        jq('.js_mobmenu').toggleClass('show');
        obj.toggleClass('open');
    };

    if (jq('.js_scrollbar').length) {
        jq('.js_scrollbar').each(function(i, item) {
            var ps = new PerfectScrollbar(item);
        });
    }

    var _mobile = function() {
        var s_w = jq(window).width(), ps = null;

        if (s_w <= BREAKPOINTS.tablet) {
            ps = new PerfectScrollbar('.js_mobmenu');

            jq(document).on('click', '.js_mobsubmenu', function(e) {
               e.preventDefault();
               var obj = jq(this);

               obj.siblings('.header__submenu').toggleClass('show');
            });

            if (jq('.js_mobmenu').length) {

            }
        } else {
            if (ps) ps.destroy();

            jq('.header__submenu').removeClass('show');
            jq('.js_mobmenu').removeClass('show');
            jq('.js_mtoggle').removeClass('open');
        }
    };

    var _listeners = function() {
        jq(window).on('load', _onLoad);

        jq(window).on('resize', _mobile);

        jq(window).on('scroll touchmove', _onScroll);

        jq(document).on('click', '[data-type="scroll"]', _scroller);

        jq(document).on('click', '.js_preview', _preview);

        jq(document).on('click', '.js_filter', _filter);

        jq(document).on('click', '.js_mtoggle', _mtoggle);
    };

    return {
        init: init
    };
})();

Pasternak.init();