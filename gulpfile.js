var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'),
    csso = require('gulp-csso'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    pug = require('gulp-pug'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    gulpIf = require('gulp-if'),
    svgSprite = require('gulp-svg-sprite'),
    notify = require("gulp-notify");

var isDev = true;

gulp.task('stylus', function() {
    return gulp.src(['src/styl/*.styl', '!src/styl/_*.styl'])
        .pipe(gulpIf(isDev, sourcemaps.init()))
        .pipe(stylus({
            'include css': true
        }))
        .on('error', notify.onError(function(error) {
          return {
            title: 'Stylus',
            message: error.message
          }
        }))
        .pipe(autoprefixer(['last 15 versions', '>1%', 'ie 8', 'ie 7'], {
            cascade: true
        }))
        .pipe(csso())
        .pipe(gulpIf(isDev, sourcemaps.write()))
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('dist/assets/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: 'dist'
        }
    });
});

gulp.task('appjs', function() {
    return gulp.src([
            'src/js/*.js'
        ])
        .pipe(concat('app.min.js'))
        .pipe(gulpIf(!isDev, uglify()))
        .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('scripts', function() {
    return gulp.src([
            'src/libs/jquery/jquery.min.js',
            'src/libs/owl/owl.carousel.min.js',
            'src/libs/owl/owl.thumbs.js',
            'src/libs/validate/jquery.validate.min.js',
            'src/libs/magnific-popup/jquery.magnific-popup.js',
            'src/libs/masked/jquery.maskedinput.js',
            'src/libs/perfect-scrollbar/perfect-scrollbar.js'
            //'src/libs/slider/js/lightslider.js',
            //'src/libs/gallery/js/lightgallery.js',

            //'src/libs/sumo-select/jquery.sumoselect.js',
            //'src/libs/jquery-ui/jquery-ui.js'
        ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('csslibs', ['stylus'], function() {
  return gulp.src([
          'src/libs/owl/assets/owl.carousel.css',
          'src/libs/magnific-popup/magnific-popup.css',
          'src/libs/perfect-scrollbar/perfect-scrollbar.css'
          //'src/libs/slider/css/lightslider.css',
          //'src/libs/gallery/css/lightgallery.css',
          //'src/libs/sumo-select/sumoselect.css',
          //'src/libs/jquery-ui/jquery-ui.css'
      ])
        .pipe(cssnano())
        .pipe(csso())
        .pipe(concat('libs.min.css'))
        .pipe(gulp.dest('dist/assets/css'));
});

// Работа с pug
gulp.task('pug', function() {
    gulp.src('src/pug/pages/*.pug')
        .pipe(pug({
            pretty: true
        }))
        .on('error', notify.onError(function(error) {
          return {
            title: 'Pug',
            message: error.message
          }
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['browser-sync', 'pug', 'csslibs', 'scripts', 'appjs', 'img', 'fonts', 'favicons'], function() {
    gulp.watch('src/styl/**/*.styl', ['stylus']);
    gulp.watch('src/pug/**/*.pug', ['pug']);
    gulp.watch('src/js/*.js', ['appjs']);
    gulp.watch('dist/*.html', browserSync.reload);
    gulp.watch('dist/assets/js/*.js', browserSync.reload);
});

gulp.task('clean', function() {
    return del.sync(['dist']);
});

gulp.task('fonts', function() {
    return gulp.src('src/assets/fonts/**/*')
        .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('favicons', function() {
    return gulp.src('src/assets/favicons/**/*')
        .pipe(gulp.dest('dist/assets/favicons'));
});

gulp.task('img', function() {
    return gulp.src('src/assets/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{
                removeViewBox: false
            }],
            une: [pngquant()]
        })))
        .pipe(gulp.dest('dist/assets/img'));
});

gulp.task('build', ['clean', 'img', 'pug', 'stylus', 'scripts', 'appjs']);

gulp.task('clear', function() {
    return cache.clearAll();
});

gulp.task('default', ['watch']);
